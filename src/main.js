const $ = require('jquery');
import "bootstrap";

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';


import "./assets/sass/app.scss";
import "./mixins";
import "./components";

Vue.config.productionTip = false;

Vue.prototype.$baseurl = 'http://54.198.203.86/api';

new Vue({
  router,
  store,
  render: h => h(App),
  mounted() {
    this.$store.dispatch('loadStore');
  },
  watch: {
    style(data) {
      if (data === 'dark') {
        $('body').addClass('dark-theme');
      }else {
        $('body').removeClass('dark-theme');
      }
    }
  }
}).$mount('#app');
