import Vue from 'vue';
import Feather from 'vue-feather';

Vue.use(Feather);

Vue.component('dropzone', require('./Dropzone.vue').default);
Vue.component('custom-tags', require('./CustomTags.vue').default);