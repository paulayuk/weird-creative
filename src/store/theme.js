export default {
	namespaced: true,
	state: {
		mode: 'light',
		sidebar: 'closed'
	},
	mutations: {
		setMode(state, { mode }) {
			if (!mode) return false;

			state.mode = mode;
		},
		setSidebar(state, { status }) {
			if (!status) return false;

			state.sidebar = status;
		},
	},
	actions: {
		changeTheme({ commit, dispatch }, { mode }) {
			commit('setMode', { mode });
			dispatch('saveStore', { module: 'theme' }, { root: true });
		},
		toggleTheme({ state, dispatch }) {
			const mode = state.mode === 'light' ? 'dark' : 'light';

			dispatch('changeTheme', { mode });
		},
		toggleSidebar({ state, commit }) {
			const status = state.sidebar === 'closed' ? 'open' : 'closed';

			commit('setSidebar', { status });
		},
		load({ dispatch }, { data }) {
			if (!data) return;
			
			dispatch('changeTheme', { mode: data.mode });
		}
	}
}