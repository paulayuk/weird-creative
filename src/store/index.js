import Vue from 'vue'
import Vuex from 'vuex'
import Cookie from 'vue-cookies';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        store_key_prefix: 'weird-creatives',
        ready: false
    },
    mutations: {
        ready(state) {
            state.ready = true;
        }
    },
    actions: {
        saveStore(store, { module }) {
            const state = JSON.parse(JSON.stringify(this.state[module]));

            const key = `${this.state.store_key_prefix}-${module}`;

            Cookie.set(key, state);
        },
        loadStore({ commit, dispatch }) {
            const modules = this._modulesNamespaceMap;

            for (let module in modules) {
                const key = `${this.state.store_key_prefix}-${module.replace(/\/$/, '')}`;
                let data = Cookie.get(key);

                dispatch(`${module}load`, { data });
            }

            commit('ready');
        }
    },
    modules: {
        session: require('./session').default,
        theme: require('./theme').default,
        resources: require('./resources').default,
    }
})
