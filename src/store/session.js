import Cookies from "vue-cookies";

export default {
	/*
	===============================================================================================================
	Allow namespacing
	===============================================================================================================
	*/
	namespaced: true,
	/*
	===============================================================================================================
	Session Based Variables
	===============================================================================================================
	*/
	state: {
		/*
		------------------------------------------------------------------
		App Authenticated State
		------------------------------------------------------------------
		*/
		loggedIn: false,
		/*
		------------------------------------------------------------------
		Authenticated API Token
		------------------------------------------------------------------
		*/
		token: null,
		/*
		------------------------------------------------------------------
		Token Keyword For Storage
		------------------------------------------------------------------
		*/
		token_name: 'weird-creatives-api-token',
		/*
		------------------------------------------------------------------
		Authenticated User Account Information
		------------------------------------------------------------------
		*/
		user: null,
		/*
		------------------------------------------------------------------
		Authenticated User Account Role
		------------------------------------------------------------------
		*/
		role: null,
		/*
		------------------------------------------------------------------
		Store Expiry Date
		------------------------------------------------------------------
		*/
		expires_at: null,
		/*
		------------------------------------------------------------------
		Storage Method
		------------------------------------------------------------------
		*/
		store_type: 'cookies',
		/*
		------------------------------------------------------------------
		Storage Timeout
		------------------------------------------------------------------
		*/
		timeout: '1h',
		/*
		------------------------------------------------------------------
		Store Ready State
		------------------------------------------------------------------
		*/
		ready: false
	},
	getters: {
		sessionExpired: state => {
			const time = state.expires_at;
            const now = (new Date()).getTime();

            return time ? now >= time : false;
		}
	},
	mutations: {
		/*
		------------------------------------------------------------------
		Save User's Data in Session
		------------------------------------------------------------------
		*/
		setUser(state, { user, token, role }) {
			const time = (new Date()).getTime() + 3600*1000;
			state.user = user;
			state.token = token;
			state.role = role;
			state.expires_at = time;
			state.loggedIn = true;
		},
		/*
		----------------------------------------------------------------------
		Remove User's Data from Session
		----------------------------------------------------------------------
		*/
		removeUser(state) {
			state.user = null;
			state.token = null;
			state.role = null;
			state.expires_at = null;
			state.loggedIn = null;
		},
		/*
		------------------------------------------------------------------
		Activate Session Module
		------------------------------------------------------------------
		*/
		ready(state) {
			state.ready = true;
		},
	},
	/*
	===============================================================================================================
	Session Based Actions
	===============================================================================================================
	*/
	actions: {
		/*
		------------------------------------------------------------------
		Activate Session Module
		------------------------------------------------------------------
		*/
		ready({ commit }) {
			commit('ready');
		},
		/*
		------------------------------------------------------------------
		Authenticate User
		------------------------------------------------------------------
		*/
		login({ commit, dispatch }, { user, token, role, remember }) {
			commit("setUser", { user, token, role });
			dispatch("saveUser", { user, token, role, remember });
			commit('ready');
		},
		/*
		------------------------------------------------------------------
		Remove Authentication for User
		------------------------------------------------------------------
		*/
		async logout({ dispatch, commit }) {
			
			commit('removeUser');
			dispatch('clearUser');

			commit('ready');
		},
		/*
		------------------------------------------------------------------
		Load User's Data from Server
		------------------------------------------------------------------
		*/
		async load({ state, dispatch, commit }) {

			const data = Cookies.get(state.token_name);

			if (data) {
				dispatch('login', { ...data });
			}

			commit('ready');
		},
		/*
		------------------------------------------------------------------
		Save User's Data from Server
		------------------------------------------------------------------
		*/
		saveUser({ state }) {
			switch (state.store_type) {
				case 'cookies':
					Cookies.set(state.token_name, state, state.timeout);
					break;
				default:
					localStorage.setItem(state.token_name, state);
					break;
			}
		},
		/*
		------------------------------------------------------------------
		Clear User's Data from Server
		------------------------------------------------------------------
		*/
		clearUser({ state }) {
			switch (state.store_type) {
				case 'cookies':
					Cookies.remove(state.token_name);
					break;
				default:
					localStorage.removeItem(state.token_name);
					break;
			}
		}
	}
}