import Vue from 'vue';
import $ from 'jquery';

Vue.mixin({
	data() {
		return {
			screenSize: $(window).width()
		}
	},
	computed: {
		isDark() {
			return this.style === 'dark';
		},
		isLight() {
			return this.style === 'light';
		},
		isSmallScreen() {
			return this.screenSize <= 768;
		},
		style() {
			return this.$store.state.theme.mode;
		},
		btnStyle() {
			return this.isLight ? 'btn-dark' : 'btn-light'
		},
		bagdeStyle() {
			return this.isLight ? 'badge-dark' : 'badge-light'
		},
		tagStyle() {
			return this.isLight ? 'tag-dark' : 'tag-light'
		},
	},
	mounted() {
		$(window).on('resize', () => {
			this.screenSize = $(window).width();
		});
	}
});