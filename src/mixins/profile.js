import Vue from 'vue';
import { mapState } from 'vuex';
import $ from 'jquery';

Vue.mixin({
	computed: {
		...mapState('session', ['loggedIn'])
	},
	methods: {
		openLoginForm() {
			$('#signup-form').modal('hide');
			$('#login-form').modal('show');
		},
		openSignUpForm() {
			$('#login-form').modal('hide');
			$('#signup-form').modal('show');
		},
	}
});