import Vue from "vue";
const feather = require('feather-icons')
const $ = require('jquery');

Vue.directive('feather', {
	inserted(el, directive) {
		$(el).attr('data-feather', directive.value);
		feather.replace();
	}
});