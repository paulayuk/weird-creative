export default [
	{
		path: '/',
		component: require('../views/landing/Layout').default,
		children: [
			{
				path: '/',
				name: 'home',
				component: require('../views/landing/Home').default
			},
			{
				path: '/about',
				name: 'about',
				component: () => import('../views/landing/About')
			},
			{
				path: '/contact',
				name: 'contact',
				component: () => import('../views/landing/Contact')
			}
		]
	}
];