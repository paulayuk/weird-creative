import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',

  // linkActiveClass: 'active',
  linkExactActiveClass: 'active',

  routes: [
    ...require('./landing').default,
    ...require('./app').default,
    ...require('./auth').default
  ]
});

router.beforeEach( async (to, from, next) => {
	var timer = 3000;
	while (!router.app.$store || !router.app.$store.state.ready) {
		if (timer === 0) {
			return alert('Store Not Ready');
		}
		await new Promise(resolve => setTimeout( resolve, 10 ) );
		timer -= 10;
	}
	next();
} );

export default router;
