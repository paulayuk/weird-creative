export default [
    {
        path: '/',
        component: () => import('../views/auth/Layout.vue'),
        children: [
            {
                path: '/login',
                name: 'login',
                component: require('../views/auth/Login.vue').default
            },
            {
                path: '/register',
                name: 'register',
                component: require('../views/auth/Register.vue').default
            },
        ]
    }
];