export default [
	{
		path: '/',
		component: () => import('../views/app/Layout.vue'),
		children: [
			{
				path: '/discover',
				name: 'discover',
				component: () => import('../views/app/discover/Index.vue')
			},
			{
				path: '/discover/:category',
				name: 'discover-single',
				component: () => import('../views/app/discover/Single.vue')
			},
			{
				path: '/competitions',
				name: 'competitions',
				component: () => import('../views/app/competitions/Index.vue')
			},
			{
				path: '/competitions/:competitionId',
				name: 'competitions-single',
				component: () => import('../views/app/competitions/Single.vue')
			},
			{
				path: '/competitions/apply/:competitionId',
				name: 'competitions-apply',
				component: () => import('../views/app/competitions/Apply.vue')
			},
			{
				path: '/account/profile',
				name: 'account-profile',
				component: () => import('../views/app/account/Profile.vue')
			},
			{
				path: '/account/update',
				name: 'account-update',
				component: () => import('../views/app/account/Update.vue')
			},
			{
				path: '/store',
				name: 'store',
				component: () => import('../views/app/e-commerce/Index.vue')
			},

			{
				path: '/dashboard',
				name: 'dashboard',
				component: () => import('../views/app/dashboard/Index.vue')
			},
			{
				path: '/dashboard/users',
				name: 'dashboard-users',
				component: () => import('../views/app/dashboard/Users.vue')
			},
			{
				path: '/dashboard/competitions',
				name: 'dashboard-competitions',
				component: () => import('../views/app/dashboard/competitions/Index.vue'),
			},
			{
				path: '/posts/new',
				name: 'posts-new',
				component: () => import('../views/app/posts/New.vue')
			},
			{
				path: '/posts/update/:postId',
				name: 'posts-update',
				component: () => import('../views/app/posts/Update.vue'),
			},
		]
	}		
];